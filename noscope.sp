#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <smlib>
#include <multicolors>

public Plugin myinfo =
{
	name = "NoScope",
	author = ".#Zipcore",
	version = "1.0",
	url = "zipcore#googlemail.com"
};

public void OnPluginStart()
{
	HookEvent("player_death", Event_OnPlayerDeath, EventHookMode_Pre);
}

public void Event_OnPlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int attacker = GetClientOfUserId(event.GetInt("attacker"));
	
	if(!client || !attacker)
		return;
	
	if(attacker == client)
		return;
	
	if(!IsClientInGame(client) || !IsClientInGame(attacker))
		return;
	
	char weapon[32];
	GetEventString(event, "weapon", weapon, sizeof(weapon));
	
	int iFOV = GetEntProp(attacker, Prop_Data, "m_iFOV");
	
	if (iFOV == 0 && (strcmp(weapon, "awp", false) == 0 || strcmp(weapon, "ssg08", false) == 0))
		CPrintToChatAll("{darkred}%N{green} noscoped {darkred}%N{green}. Distance: {darkred}%.2f units{green}.", attacker, client, Entity_GetDistance(client, attacker));
}